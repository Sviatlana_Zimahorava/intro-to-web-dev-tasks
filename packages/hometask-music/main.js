import "./styles/style.css";

import featureBackgroundImage from "./assets/images/music-titles.png";
import formBackgroundImage from "./assets/images/background-page-sign-up.png";

function createHeader() {
  /* global document */
  const header = document.createElement("header");
  header.classList.add("page-header");

  header.innerHTML = `
        <a href="#" class="page-logo">
            Simo
        </a>
        <nav class="page-nav">
            <ul class="page-nav__list">
                <li class="page-nav__item">
                    <a href="#" id="discover">Discover</a>
                </li>
                <li class="page-nav__item">
                    <a href="#" id="join">Join</a>
                </li>
                <li class="page-nav__item">
                    <a href="#">Sign In</a>
                </li>
            </ul>
        </nav>
    `;

  return header;
}

function createMain() {
  const main = document.createElement("main");
  main.classList.add("page-main");

  return main;
}

function createLanding() {
  const landing = document.createElement("section");
  landing.classList.add("landing");

  landing.innerHTML = `
    <h1 class="landing__header">Feel the music</h1>
    <p class="landing__text">Stream over 10 million songs with one click</p>
    <a class="btn  landing__btn" href="#" id="joinNowButton">Join now</a>
  `;

  return landing;
}

function createFeature() {
  const feature = document.createElement("section");
  feature.classList.add("feature");

  feature.innerHTML = `
      <h1 class="feature__header">Discover new music</h1>
      <ul class="feature__list">
        <li class="feature__item">
            <a class="btn  feature__btn" href="#">Charts</a>
        </li>
        <li class="feature__item">
            <a class="btn  feature__btn" href="#">Songs</a>
        </li>
        <li class="feature__item">
            <a class="btn  feature__btn" href="#">Artists</a>
        </li>
      </ul>
      <p class="feature__text">By joing you can benefit by listening to the latest albums released</p>
    `;

  return feature;
}

function createForm() {
  const form = document.createElement("section");
  form.classList.add("page-form");

  form.innerHTML = `
        <form class="form" method="POST" action="/">
            <div class="form__wrapper">
                <div class="form__row">
                    <label for="name">Name:</label>
                    <input type="text" name="name" id="name" required>
                </div>
                <div class="form__row">
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="password" required>
                </div>
                <div class="form__row">
                    <label for="email">Email:</label>
                    <input type="email" name="email" id="email" required>
                </div>
            </div>
            <input class="btn  form__btn" type="submit" value="Join now">
        </form>
      `;

  return form;
}
function createFooter() {
  const footer = document.createElement("footer");
  footer.classList.add("page-footer");

  footer.innerHTML = `
    <nav class="page-footer__nav">
        <ul class="page-footer__list">
            <li class="page-footer__item">
                <a href="#">About As</a>
            </li>
            <li class="page-footer__item">
                <a href="#">Contact</a>
            </li>
            <li class="page-footer__item">
                <a href="#">CR Info</a>
            </li>
            <li class="page-footer__item">
                <a href="#">Twitter</a>
            </li>
            <li class="page-footer__item">
                <a href="#">Facebook</a>
            </li>
        </ul>
    </nav>
    `;

  return footer;
}

const header = createHeader();
const main = createMain();
const landing = createLanding();
const feature = createFeature();
const form = createForm();
const footer = createFooter();

main.append(landing);

const app = document.querySelector("#app");
app.append(header);
app.append(main);
app.append(footer);

const displayAllHeaderElements = () => {
  const links = header.querySelectorAll("a");
  links.forEach(link => {
    if (link.classList.contains("hidden")) {
      link.classList.remove("hidden");
    }
  });
};

const featureHeaderLink = header.querySelector("#discover");
const renderFeaturePage = () => {
  main.innerHTML = "";
  main.append(feature);
  displayAllHeaderElements();
  app.style.background = `url(${featureBackgroundImage}) no-repeat center right content-box`;
  featureHeaderLink.classList.add("hidden");
  featureHeaderLink.removeEventListener("click", renderFeaturePage);
};
featureHeaderLink.addEventListener("click", renderFeaturePage);

const joinHeaderLink = header.querySelector("#join");
const joinNowButton = landing.querySelector("#joinNowButton");
const renderSignUpPage = () => {
  main.innerHTML = "";
  main.append(form);
  displayAllHeaderElements();
  app.style.background = `url(${formBackgroundImage}) no-repeat bottom right`;
  joinHeaderLink.classList.add("hidden");
  joinHeaderLink.removeEventListener("click", renderSignUpPage);
  joinNowButton.removeEventListener("click", renderSignUpPage);
};
joinHeaderLink.addEventListener("click", renderSignUpPage);
joinNowButton.addEventListener("click", renderSignUpPage);
